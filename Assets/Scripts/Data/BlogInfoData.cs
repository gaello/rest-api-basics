﻿/// <summary>
/// Blog Info is data model for server response to GetBlogInfo.
/// </summary>
[System.Serializable]
public class BlogInfoData
{
    public string blogAddress;
    public string bloggerName;
    public int bloggerAge;
}
