﻿/// <summary>
/// Quote is data model for server response to GetQuote.
/// </summary>
[System.Serializable]
public class QuoteData
{
    public string quote;
    public string author;
}
