﻿/// <summary>
/// This class store server config keys and urls.
/// </summary>
public class ServerConfig
{
    // URL with place to put API method in it.
    public const string SERVER_API_URL_FORMAT = "http://www.mocky.io/v2/{0}";

    // Mocky generates random strings for your endpoints, you should name them properly!
    public const string API_GET_QUOTE = "5cb15fdf330000ee1557204f";
    public const string API_GET_BLOG_INFO = "5cb28ba13000007b00a78c92";
}
